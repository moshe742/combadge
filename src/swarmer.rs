use async_std::{io, task};
use futures::{future, prelude::*};
use libp2p::{
    floodsub::{self, Floodsub, FloodsubEvent},
    identity,
    mdns::{Mdns, MdnsEvent},
    swarm::NetworkBehaviourEventProcess,
    Multiaddr, NetworkBehaviour, PeerId, Swarm,
};
use std::{
    error::Error,
    task::{Context, Poll},
};

#[derive(NetworkBehaviour)]
pub struct MyBehaviour {
    pub floodsub: Floodsub,
    pub mdns: Mdns,
    // Struct fields which do not implement NetworkBehaviour need to be ignored
    #[behaviour(ignore)]
    #[allow(dead_code)]
    ignored_member: bool,
}

impl MyBehaviour {
    pub fn new(floodsub: Floodsub, mdns: Mdns) -> MyBehaviour {
        MyBehaviour {
            floodsub: floodsub,
            mdns: mdns,
            ignored_member: false,
        }
    }
}

impl NetworkBehaviourEventProcess<FloodsubEvent> for MyBehaviour {
    // Called when `floodsub` produces an event.
    fn inject_event(&mut self, message: FloodsubEvent) {
        if let FloodsubEvent::Message(message) = message {
            println!(
                "Received: '{:?}' from {:?}",
                String::from_utf8_lossy(&message.data),
                message.source
            );
        }
    }
}

impl NetworkBehaviourEventProcess<MdnsEvent> for MyBehaviour {
    // Called when `mdns` produces an event.
    fn inject_event(&mut self, event: MdnsEvent) {
        match event {
            MdnsEvent::Discovered(list) => {
                for (peer, _) in list {
                    self.floodsub.add_node_to_partial_view(peer);
                }
            }

            MdnsEvent::Expired(list) => {
                for (peer, _) in list {
                    if !self.mdns.has_node(&peer) {
                        self.floodsub.remove_node_from_partial_view(&peer);
                    }
                }
            }
        }
    }
}

pub fn get_swarm(floodsub_topic: floodsub::Topic) -> Swarm<MyBehaviour> {
    // create a random key pair
    let local_key = identity::Keypair::generate_ed25519();
    let local_peer_id = PeerId::from(local_key.public());
    println!("Local peer id: {:?}", local_peer_id);

    let mdns = Mdns::new().unwrap();
    let local_peer_id = PeerId::from(local_key.public());

    // setup an encrypted DNS enabled TCP transport over the Mplex and yamux protocols
    let transport = libp2p::build_development_transport(local_key).unwrap();

    let mut behaviour = MyBehaviour::new(Floodsub::new(local_peer_id.clone()), mdns);

    behaviour.floodsub.subscribe(floodsub_topic);
    Swarm::new(transport, behaviour, local_peer_id)
}

pub fn run_swarm(
    floodsub_topic: floodsub::Topic,
    ip: &str,
    port: &str,
) -> Result<(), Box<dyn Error>> {
    // Create a Swarm to manage peers and events
    let mut swarm = get_swarm(floodsub_topic.clone());

    // Reach out to another node if specified
    if ip != "0.0.0.0" {
        let ip_string = format!("/ip4/{}/tcp/{}", ip, port);
        let addr: Multiaddr = ip_string.parse()?;
        Swarm::dial_addr(&mut swarm, addr)?;
        println!("Dialed {:?}", ip_string)
    }

    // Read full lines from stdin
    // I think this should be replaced with IPC for the real thing, but need to think about it
    let mut stdin = io::BufReader::new(io::stdin()).lines();

    // Listen on all interfaces and whatever port the OS assigns
    Swarm::listen_on(&mut swarm, "/ip4/0.0.0.0/tcp/0".parse()?)?;

    // Kick it off
    let mut listening = false;
    task::block_on(future::poll_fn(move |cx: &mut Context<'_>| {
        loop {
            match stdin.try_poll_next_unpin(cx)? {
                Poll::Ready(Some(line)) => swarm
                    .floodsub
                    .publish(floodsub_topic.clone(), line.as_bytes()),
                Poll::Ready(None) => panic!("Stdin closed"),
                Poll::Pending => break,
            }
        }
        loop {
            match swarm.poll_next_unpin(cx) {
                Poll::Ready(Some(event)) => println!("{:?}", event),
                Poll::Ready(None) => return Poll::Ready(Ok(())),
                Poll::Pending => {
                    if !listening {
                        for addr in Swarm::listeners(&swarm) {
                            println!("Listening on {:?}", addr);
                            listening = true;
                        }
                    }
                    break;
                }
            }
        }
        Poll::Pending
    }))
}
