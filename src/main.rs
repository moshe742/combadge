use clap::{App, Arg};
use libp2p::floodsub;
use std::error::Error;

mod swarmer;

fn main() -> Result<(), Box<dyn Error>> {
    env_logger::init();

    let matches = App::new("comdish")
        .arg(
            Arg::with_name("ip")
                .short("i")
                .long("ip")
                .value_name("IP")
                .help("The ip to use")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("port")
                .short("p")
                .long("port")
                .value_name("PORT")
                .help("The port to use, optional")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("topic")
                .short("t")
                .long("topic")
                .value_name("TOPIC")
                .help("The topic to use")
                .takes_value(true)
                .required(true),
        )
        .get_matches();

    let ip = matches.value_of("ip").unwrap_or("0.0.0.0");
    // may be something with 47?
    let port = matches.value_of("port").unwrap_or("20500");
    let topic = matches.value_of("topic").unwrap();
    let floodsub_topic = floodsub::Topic::new(topic.clone());

    swarmer::run_swarm(floodsub_topic, &ip, &port)
}
